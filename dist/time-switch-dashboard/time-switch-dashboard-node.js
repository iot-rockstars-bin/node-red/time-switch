"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const html = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .week-configuaration {
        white-space: nowrap;
        display: flex;
    }
    .week-configuration-collumn {
        padding: 4px;
    }

    .week-configuration-collumn-1 {
        min-width: 95px;
    }

    .week-configuration-collumn-2 {
        width: 60px;
    }

    .week-configuration-collumn-3 {
        min-width: 10px;
    }

    .week-configuration-collumn-4 {
        min-width: 95px;
    }

    .week-configuration-collumn-5 {
        min-width: 60px;
    }

    .week-configuration-collumn-6 {
        min-width: 80px;
    }

    .week-configuration-collumn-7 {
        min-width: 20px;
        cursor: pointer;
        opacity: 0.5;
    }

    .week-configuration-collumn-7:hover {
        opacity: 1;
    }

    .defaul-scene-index-form {
        padding-top: 25px;
    }

    .default-scene-index {
        padding-top: 25px;
    }

    .configure-button {
        width: 120px;
        margin-top: 25px!important;
    }
</style>
<div ng-repeat="value in config.week" class="week-configuaration">
    <div class="week-configuration-collumn week-configuration-collumn-1">{{getDayFromSecondsOfWeek(value.startInclusive)}}</div>
    <div class="week-configuration-collumn week-configuration-collumn-2">{{secondsToHumanReadable(value.startInclusive)}}</div>
    <div class="week-configuration-collumn week-configuration-collumn-3">-</div>
    <div class="week-configuration-collumn week-configuration-collumn-4">{{getDayFromSecondsOfWeek(value.startInclusive)}}</div>
    <div class="week-configuration-collumn week-configuration-collumn-5">{{secondsToHumanReadable(value.endExclusive)}}</div>
    <div class="week-configuration-collumn week-configuration-collumn-6">Scene: {{value.sceneId}}</div>
    <div ng-click="(removeScene(value))" class="week-configuration-collumn week-configuration-collumn-7"><i class="fa fa-trash-o"></i></div>
</div>
<div class="default-scene-index">Default scene index: <strong>{{config.defaultSceneId}}</strong></div>

<md-button class="configure-button" ng-click="toggleConfiguration()">
    <i class="fa fa-cogs"></i>
    Configure
</md-button>
<div ng-if="showConfiguration">
    <form class="defaul-scene-index-form">
        <md-input-container>
            <label>Default Scene id:</label>
            <input type="string" id="defaultscene-index-input" name="input-defaukt-scene" ng-model="config.defaultSceneId" ng-change="sendConfig()" required />
        </md-input-container>
    </form>
    <form style="padding: 10px;">
        <md-input-container>
            <md-select ng-model="newScene.day" placeholder="Day" class="md-no-underline" required>
                <md-option value="0">Monday</md-option>
                <md-option value="1">Tuesday</md-option>
                <md-option value="2">Wednesday</md-option>
                <md-option value="3">Thursday</md-option>
                <md-option value="4">Friday</md-option>
                <md-option value="5">Saturday</md-option>
                <md-option value="6">Sunday</md-option>
            </md-select>
        </md-input-container>
        <md-input-container>
            <label>Startdate:</label>
            <input type="time" id="start-date" name="input-start-date" ng-model="newScene.startDate" placeholder="HH:mm"
                   required />
        </md-input-container>
        <md-input-container>
            <label>EndDate:</label>
            <input type="time" id="end-date" name="input-end-date" ng-model="newScene.endDate" placeholder="HH:mm"
                   required />
        </md-input-container>
        <md-input-container>
            <label>Scene id:</label>
            <input type="string" id="scene-index" name="input-scene" ng-model="newScene.sceneId" required />
        </md-input-container>
        <md-button type="submit" ng-click="click()">Add Scene</md-button>
    </form>
</div>

`;
module.exports = function (RED) {
    function TimeSwitchDashboardNode(config) {
        RED.nodes.createNode(this, config);
        const node = this;
        const ui = RED.require('node-red-dashboard')(RED);
        config.tcol = ui.getTheme()['group-textColor'].value || '#1bbfff';
        // Initialize node
        // Generate HTML/Angular code
        // Initialize Node-RED Dashboard widget
        // see details: https://github.com/node-red/node-red-ui-nodes/blob/master/docs/api.md
        const done = ui.addWidget({
            node: node,
            order: config.order,
            group: config.group,
            width: config.width,
            height: config.height,
            format: html,
            templateScope: 'local',
            emitOnlyNewValues: false,
            forwardInputMessages: false,
            storeFrontEndInputAsState: false,
            initController: function ($scope, _events) {
                $scope.newScene = {};
                // initialize $scope.click to send clicked widget item
                // used as ng-click="click(item, selected)"
                $scope.click = function () {
                    $scope.config.week.push($scope.getNewScene());
                    $scope.sendConfig();
                };
                $scope.toggleConfiguration = function () {
                    if ($scope.showConfiguration === undefined) {
                        $scope.showConfiguration = true;
                    }
                    else {
                        $scope.showConfiguration = !$scope.showConfiguration;
                    }
                };
                $scope.removeScene = function (scene) {
                    const index = $scope.config.week.indexOf(scene);
                    if (index > -1) {
                        $scope.config.week.splice(index, 1);
                        $scope.sendConfig();
                    }
                };
                $scope.sendConfig = function () {
                    $scope.send({ payload: angular.toJson($scope.config) });
                };
                $scope.$watch('msg', function (msg) {
                    if (msg === undefined) {
                        $scope.config = {
                            week: []
                        };
                    }
                    else {
                        $scope.config = JSON.parse(msg.items);
                        if ($scope.config.week !== undefined) {
                            $scope.config.week.sort(function (a, b) {
                                return a.startInclusive - b.startInclusive;
                            });
                        }
                    }
                });
                class DayOfWeek {
                    constructor(index, name) {
                        this.index = index;
                        this.name = name;
                    }
                    static getDayByIndex(index) {
                        return this.WEEK.find(day => day.index === index);
                    }
                }
                DayOfWeek.MONDAY = new DayOfWeek(0, 'Monday');
                DayOfWeek.TUESDAY = new DayOfWeek(1, 'Tuesday');
                DayOfWeek.WEDNESDAY = new DayOfWeek(2, 'Wednesday');
                DayOfWeek.THURSDAY = new DayOfWeek(3, 'Thursday');
                DayOfWeek.FRIDAY = new DayOfWeek(4, 'Friday');
                DayOfWeek.SATURDAY = new DayOfWeek(5, 'Saturday');
                DayOfWeek.SUNDAY = new DayOfWeek(6, 'Sunday');
                DayOfWeek.WEEK = [DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY];
                $scope.secondsToHumanReadable = function (secondsOfWeek) {
                    const secondsOfDay = secondsOfWeek % 86400;
                    const hours = String(Math.trunc(secondsOfDay / 3600)).padStart(2, '0');
                    const minutes = String(Math.trunc((secondsOfDay % 3600) / 60)).padStart(2, '0');
                    return `${hours}:${minutes}`;
                };
                $scope.getDayFromSecondsOfWeek = function (secondsOfWeek) {
                    var _a;
                    return (_a = DayOfWeek.getDayByIndex(Math.trunc(secondsOfWeek / 86400))) === null || _a === void 0 ? void 0 : _a.name;
                };
                $scope.getNewScene = function () {
                    console.log($scope.newScene);
                    const daySeconds = $scope.newScene.day * 86400;
                    const startSeconds = $scope.newScene.startDate.getHours() * 60 * 60 + $scope.newScene.startDate.getMinutes() * 60 + daySeconds;
                    const endSeconds = $scope.newScene.endDate.getHours() * 60 * 60 + $scope.newScene.endDate.getMinutes() * 60 + daySeconds;
                    return {
                        startInclusive: startSeconds,
                        endExclusive: endSeconds,
                        sceneId: $scope.newScene.sceneId,
                    };
                };
            },
            convertBack: function (value) {
                return value;
            },
            beforeEmit: function (msg, value) {
                // make msg.payload accessible as msg.items in widget
                return { msg: { items: value } };
            },
            beforeSend: function (msg, orig) {
                if (orig) {
                    return orig.msg;
                }
            },
        });
        node.on('close', done);
    }
    RED.nodes.registerType('ui-time-switch-dashboard', TimeSwitchDashboardNode);
};
//# sourceMappingURL=time-switch-dashboard-node.js.map