"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const time_switch_config_1 = require("./time-switch-config");
const time_switch_controller_1 = require("./time-switch.controller");
module.exports = function (RED) {
    function TimeSwitchNode(config) {
        const controller = new time_switch_controller_1.TimeSwitchController({
            week: [],
            specialDays: [],
            dayPatterns: [],
            defaultSceneId: '1'
        });
        this.on('input', (msg, send, done) => __awaiter(this, void 0, void 0, function* () {
            try {
                const oldConfig = controller.config;
                const config = msg.payload;
                if (JSON.stringify(oldConfig) !== JSON.stringify(config)) {
                    controller.setConfig(config);
                }
                done();
            }
            catch (err) {
                done(err);
            }
        }));
        const intervalHandle = setInterval(() => __awaiter(this, void 0, void 0, function* () {
            const state = yield controller.input({});
            this.send({ payload: state, topic: time_switch_config_1.TIME_SWITCH_OUTPUT_TOPIC });
            this.context().set('state', state);
        }), 10000);
        this.on('close', function () {
            clearInterval(intervalHandle);
        });
    }
    RED.nodes.registerType('timeSwitch', TimeSwitchNode);
};
//# sourceMappingURL=time-switch-node.js.map