export interface TimeSwitchOutput {
    /**
     * The scene which is currently active.
     * If a special day is active the configuration will come from the special day, otherwise from the normal week configuration
     */
    activeScene: string;
    /**
     * The scenes which will be active in the future. If special days are active they are merged with the week configuration so there are no gaps
     */
    futureScenes: FutureScene[];
}
export interface FutureScene {
    /**
     * The scene index which will be active in the given timespan
     */
    sceneId: string;
    /**
     * In how many seconds will this scene be active
     */
    secondsTilDeactivation: number;
    /**
     * In how many seconds will this scene be inactive again
     */
    secondsTilActivation: number;
}
