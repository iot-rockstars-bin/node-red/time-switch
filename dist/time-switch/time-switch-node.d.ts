import { NodeDef } from 'node-red';
import { TimeSwitchConfiguration } from './time-switch-config';
export interface TimeSwitchNodeConfig extends TimeSwitchConfiguration, NodeDef {
}
