"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const time_switch_controller_1 = require("./time-switch.controller");
describe('time switch controller', () => {
    it('should return correct feature for the beginning of the week', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const timeSpanWithScene = { startInclusive: 100, endExclusive: 200, sceneId: '2' };
        Object.defineProperty(timeSwitchController, 'secondsOfWeek', {
            get: jest.fn(() => 86400),
            set: jest.fn()
        });
        const futureScene = timeSwitchController.toFutureScene(timeSpanWithScene);
        expect(futureScene.sceneId).toBe('2');
        expect(futureScene.secondsTilActivation).toBe(518500);
    });
    it('should return the scenes for the next 24 hours without special', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [
                {
                    startInclusive: 200,
                    endExclusive: 3000,
                    sceneId: '1'
                },
                {
                    startInclusive: 3000,
                    endExclusive: 5123,
                    sceneId: '2'
                },
                {
                    startInclusive: 5123,
                    endExclusive: 8000,
                    sceneId: '3'
                }
            ],
            specialDays: [],
            defaultSceneId: '0',
            dayPatterns: [],
        });
        Object.defineProperty(timeSwitchController, 'secondsOfWeek', {
            get: jest.fn(() => 100),
            set: jest.fn()
        });
        const futureScenes = timeSwitchController.getFutureScenes(86400);
        expect(futureScenes.length).toBe(4);
        expect(futureScenes[0].sceneId).toBe('1');
        expect(futureScenes[0].secondsTilActivation).toBe(100);
        expect(futureScenes[0].secondsTilDeactivation).toBe(2900);
        expect(futureScenes[1].sceneId).toBe('2');
        expect(futureScenes[1].secondsTilActivation).toBe(2900);
        expect(futureScenes[1].secondsTilDeactivation).toBe(5023);
        expect(futureScenes[2].sceneId).toBe('3');
        expect(futureScenes[2].secondsTilActivation).toBe(5023);
        expect(futureScenes[2].secondsTilDeactivation).toBe(7900);
        expect(futureScenes[3].sceneId).toBe('0');
        expect(futureScenes[3].secondsTilActivation).toBe(7900);
    });
    const futureScene = {
        secondsTilActivation: 100,
        secondsTilDeactivation: 2900,
        sceneId: '1',
    };
    const futureScenes = [futureScene];
    it('should return the scenes for the next 24 hours without week configuration', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const spySpecial = jest.spyOn(timeSwitchController, 'getFutureScenesFromSpecialDays').mockImplementation(() => futureScenes);
        const spyWeek = jest.spyOn(timeSwitchController, 'getFutureScenesFromWeek').mockImplementation(() => []);
        const result = timeSwitchController.getFutureScenes(86400);
        expect(result.length).toBe(1);
        expect(result[0].sceneId).toBe('1');
        expect(result[0].secondsTilActivation).toBe(100);
        expect(result[0].secondsTilDeactivation).toBe(2900);
        spySpecial.mockRestore();
        spyWeek.mockRestore();
    });
    it('should return the scenes for the next 24 hours without week configuration', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const spySpecial = jest.spyOn(timeSwitchController, 'getFutureScenesFromSpecialDays').mockImplementation(() => futureScenes);
        const spyWeek = jest.spyOn(timeSwitchController, 'getFutureScenesFromWeek').mockImplementation(() => futureScenes);
        const spyMerge = jest.spyOn(timeSwitchController, 'mergeFutureScenes').mockImplementation(() => undefined);
        timeSwitchController.getFutureScenes(86400);
        spySpecial.mockRestore();
        spyWeek.mockRestore();
        expect(spyMerge).toHaveBeenCalledTimes(1);
        spyMerge.mockRestore();
    });
    it('should return the default configuration when nothing is configured', () => __awaiter(void 0, void 0, void 0, function* () {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [],
            specialDays: [],
            defaultSceneId: '25',
            dayPatterns: [],
        });
        expect((yield timeSwitchController.input(undefined)).activeScene).toBe('25');
    }));
    it('should return the correct configuration on input', () => __awaiter(void 0, void 0, void 0, function* () {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [
                {
                    startInclusive: 0,
                    endExclusive: 200,
                    sceneId: '7'
                },
                {
                    startInclusive: 200,
                    endExclusive: 3000,
                    sceneId: '1'
                },
                {
                    startInclusive: 3000,
                    endExclusive: 5123,
                    sceneId: '2'
                },
                {
                    startInclusive: 5123,
                    endExclusive: 8000,
                    sceneId: '3'
                },
            ],
            specialDays: [],
            defaultSceneId: '0',
            dayPatterns: [],
        });
        Object.defineProperty(timeSwitchController, 'secondsOfWeek', {
            get: jest.fn(() => 100),
            set: jest.fn()
        });
        const result = (yield timeSwitchController.input(undefined));
        const futureScenes = result.futureScenes;
        expect(futureScenes.length).toBe(4);
        expect(futureScenes[0].sceneId).toBe('1');
        expect(futureScenes[0].secondsTilActivation).toBe(100);
        expect(futureScenes[1].sceneId).toBe('2');
        expect(futureScenes[1].secondsTilActivation).toBe(2900);
        expect(futureScenes[2].sceneId).toBe('3');
        expect(futureScenes[2].secondsTilActivation).toBe(5023);
        expect(futureScenes[3].sceneId).toBe('0');
        expect(futureScenes[3].secondsTilActivation).toBe(7900);
        expect(result.activeScene).toBe('7');
    }));
    it('should return the correct seconds of the week for monday', () => {
        // https://stackoverflow.com/questions/28504545/how-to-mock-a-constructor-like-new-date
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const mockDate = new Date(2019, 11, 16, 3, 20, 3);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        expect(timeSwitchController.secondsOfWeek).toBe(3 * 60 * 60 + 20 * 60 + 3);
        spy.mockRestore();
    });
    it('should return the correct seconds of the week for sunday', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const mockDate = new Date(2019, 11, 22, 13, 54, 3);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        expect(timeSwitchController.secondsOfWeek).toBe(6 * 24 * 60 * 60 + 13 * 60 * 60 + 54 * 60 + 3);
        spy.mockRestore();
    });
    it('should return the correct seconds of the day for 13 o clock', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const mockDate = new Date(2019, 11, 22, 13, 54, 3);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        expect(timeSwitchController.secondsOfDay).toBe(13 * 60 * 60 + 54 * 60 + 3);
        spy.mockRestore();
    });
    it('should merge the futureScenes correctly with overlapping futureScenes', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [],
            dayPatterns: [],
            specialDays: [],
            defaultSceneId: '3'
        });
        const futureScenesFromSpecialDay = [];
        const futureScenesFromSWeek = [];
        futureScenesFromSpecialDay.push({
            secondsTilActivation: 0,
            secondsTilDeactivation: 300,
            sceneId: '7'
        });
        futureScenesFromSpecialDay.push({
            secondsTilActivation: 500,
            secondsTilDeactivation: 800,
            sceneId: '9'
        });
        futureScenesFromSpecialDay.push({
            secondsTilActivation: 900,
            secondsTilDeactivation: 1000,
            sceneId: '10'
        });
        futureScenesFromSWeek.push({
            secondsTilActivation: 200,
            secondsTilDeactivation: 1000,
            sceneId: '1'
        });
        futureScenesFromSWeek.push({
            secondsTilActivation: 910,
            secondsTilDeactivation: 950,
            sceneId: '9'
        });
        const result = timeSwitchController.mergeFutureScenes(futureScenesFromSpecialDay, futureScenesFromSWeek).sort((a, b) => a.secondsTilActivation - b.secondsTilActivation);
        expect(result.length).toBe(5);
        expect(result[0].sceneId).toBe('7');
        expect(result[0].secondsTilActivation).toBe(0);
        expect(result[0].secondsTilDeactivation).toBe(300);
        expect(result[1].sceneId).toBe('1');
        expect(result[1].secondsTilActivation).toBe(300);
        expect(result[1].secondsTilDeactivation).toBe(500);
        expect(result[2].sceneId).toBe('9');
        expect(result[2].secondsTilActivation).toBe(500);
        expect(result[2].secondsTilDeactivation).toBe(800);
        expect(result[3].sceneId).toBe('1');
        expect(result[3].secondsTilActivation).toBe(800);
        expect(result[3].secondsTilDeactivation).toBe(900);
        expect(result[4].sceneId).toBe('10');
        expect(result[4].secondsTilActivation).toBe(900);
        expect(result[4].secondsTilDeactivation).toBe(1000);
    });
    it('sceneIsBetween should return true when scene is within a other scene', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 200, secondsTilDeactivation: 400, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.sceneIsBetween(a, b)).toBe(true);
    });
    it('sceneIsBetween should return true when scenes start with the same time', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 200, secondsTilDeactivation: 400, sceneId: '3' };
        const b = { secondsTilActivation: 200, secondsTilDeactivation: 400, sceneId: '2' };
        expect(timeSwitchController.sceneIsBetween(a, b)).toBe(true);
    });
    it('sceneIsBetween should return false when scene overlaps at the beginning', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 50, secondsTilDeactivation: 400, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.sceneIsBetween(a, b)).toBe(false);
    });
    it('sceneIsBetween should return false when scene overlaps at the end', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 200, secondsTilDeactivation: 600, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.sceneIsBetween(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtBeginning should return true when scene overlaps at the beginning', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 50, secondsTilDeactivation: 400, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtBeginning(a, b)).toBe(true);
    });
    it('doesSceneOverlapsAtBeginning should return false when scene are equal', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtBeginning(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtBeginning should return false when the scene overlaps at the end', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 100, secondsTilDeactivation: 600, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtBeginning(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtBeginning should return false when the scene ends before b starts', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 300, secondsTilDeactivation: 500, sceneId: '3' };
        const b = { secondsTilActivation: 900, secondsTilDeactivation: 1000, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtBeginning(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtBeginning should return false when the scene ends after b ends', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 900, secondsTilDeactivation: 1000, sceneId: '3' };
        const b = { secondsTilActivation: 300, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtBeginning(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtEnd should return true when scene overlaps at the end', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 100, secondsTilDeactivation: 6000, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtEnd(a, b)).toBe(true);
    });
    it('doesSceneOverlapsAtEnd should return false when scene overlaps at the start', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 70, secondsTilDeactivation: 6000, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtEnd(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtEnd should return false when scenes are equal', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '3' };
        const b = { secondsTilActivation: 100, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtEnd(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtEnd should return false when scene ends before start of other scene', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 300, secondsTilDeactivation: 500, sceneId: '3' };
        const b = { secondsTilActivation: 900, secondsTilDeactivation: 1000, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtEnd(a, b)).toBe(false);
    });
    it('doesSceneOverlapsAtEnd should return false when scene starts after end of other scene', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const a = { secondsTilActivation: 900, secondsTilDeactivation: 1000, sceneId: '3' };
        const b = { secondsTilActivation: 300, secondsTilDeactivation: 500, sceneId: '2' };
        expect(timeSwitchController.doesSceneOverlapsAtEnd(a, b)).toBe(false);
    });
    function mockSpecialDayToFutureScene(timeSwitchController) {
        const mockDate = new Date(2020, 7, 13, 12, 0, 0);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const specialDay = {
            dayPatternId: 1,
            activationDate: new Date(2020, 7, 13, 0, 0, 0),
            name: 'Special Day'
        };
        const result = timeSwitchController.specialDayToFutureScene(specialDay);
        spy.mockRestore();
        return result;
    }
    it('specialDayToFutureScene should correctly convert the day to a futureScene at the start of the day', () => {
        const config = {
            specialDays: [],
            dayPatterns: [
                {
                    day: [
                        {
                            startInclusive: 200,
                            endExclusive: 40000,
                            sceneId: '5'
                        },
                        {
                            startInclusive: 40000,
                            endExclusive: 86000,
                            sceneId: '4'
                        }
                    ],
                    id: 1
                }
            ],
            defaultSceneId: '1',
            week: [],
        };
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(config);
        const result = mockSpecialDayToFutureScene(timeSwitchController);
        expect(result.length).toBe(4);
        expect(result[0].sceneId).toBe(config.defaultSceneId);
        expect(result[0].secondsTilActivation).toBe(0);
        expect(result[0].secondsTilDeactivation).toBe(200);
        expect(result[1].sceneId).toBe('5');
        expect(result[1].secondsTilActivation).toBe(200);
        expect(result[1].secondsTilDeactivation).toBe(40000);
        expect(result[2].sceneId).toBe('4');
        expect(result[2].secondsTilActivation).toBe(40000);
        expect(result[2].secondsTilDeactivation).toBe(86000);
        expect(result[3].sceneId).toBe('1');
        expect(result[3].secondsTilActivation).toBe(86000);
        expect(result[3].secondsTilDeactivation).toBe(86400);
    });
    it('specialDayToFutureScene should return an empty array when no day pattern is found', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            specialDays: [],
            dayPatterns: [],
            defaultSceneId: '1',
            week: [],
        });
        const result = mockSpecialDayToFutureScene(timeSwitchController);
        expect(result.length).toBe(0);
    });
    it('fillPatternWithDefaultScene should fill the day correctly with default scenes', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            defaultSceneId: '1',
            week: [],
            specialDays: [],
            dayPatterns: []
        });
        const dayPattern = {
            day: [
                {
                    startInclusive: 300,
                    endExclusive: 40000,
                    sceneId: '3'
                },
                {
                    startInclusive: 60000,
                    endExclusive: 86000,
                    sceneId: '4'
                }
            ],
            id: 7
        };
        dayPattern.day = timeSwitchController.fillPatternWithDefaultScene(dayPattern.day, 86400);
        dayPattern.day.sort((a, b) => a.startInclusive - b.startInclusive);
        expect(dayPattern.day.length).toBe(5);
        expect(dayPattern.day[0].startInclusive).toBe(0);
        expect(dayPattern.day[0].endExclusive).toBe(300);
        expect(dayPattern.day[0].sceneId).toBe('1');
        expect(dayPattern.day[1].startInclusive).toBe(300);
        expect(dayPattern.day[1].endExclusive).toBe(40000);
        expect(dayPattern.day[1].sceneId).toBe('3');
        expect(dayPattern.day[2].startInclusive).toBe(40000);
        expect(dayPattern.day[2].endExclusive).toBe(60000);
        expect(dayPattern.day[2].sceneId).toBe('1');
        expect(dayPattern.day[3].startInclusive).toBe(60000);
        expect(dayPattern.day[3].endExclusive).toBe(86000);
        expect(dayPattern.day[3].sceneId).toBe('4');
        expect(dayPattern.day[4].startInclusive).toBe(86000);
        expect(dayPattern.day[4].endExclusive).toBe(86400);
        expect(dayPattern.day[4].sceneId).toBe('1');
    });
    it('getFutureScenesFromSpecialDays should return the future scenes from the config for the next 86400 seconds', () => {
        const mockDate = new Date(2020, 7, 13, 12);
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            defaultSceneId: '1',
            week: [],
            specialDays: [{
                    activationDate: new Date(2020, 7, 13, 0, 0, 0),
                    dayPatternId: 7,
                    name: 'name_1'
                },
                {
                    activationDate: new Date(2020, 7, 14, 0, 0, 0),
                    dayPatternId: 6,
                    name: 'name_2'
                }
            ],
            dayPatterns: [{
                    day: [
                        {
                            startInclusive: 300,
                            endExclusive: 40000,
                            sceneId: '3'
                        },
                        {
                            startInclusive: 60000,
                            endExclusive: 86000,
                            sceneId: '4'
                        },
                    ],
                    id: 7
                },
                {
                    day: [{
                            startInclusive: 0,
                            endExclusive: 86400,
                            sceneId: '7'
                        }],
                    id: 6
                }]
        });
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.getFutureScenesFromSpecialDays(86400);
        spy.mockRestore();
        expect(result.length).toBe(3);
        expect(result[0].secondsTilActivation).toBe(16800);
        expect(result[0].secondsTilDeactivation).toBe(42800);
        expect(result[0].sceneId).toBe('4');
        expect(result[1].secondsTilActivation).toBe(42800);
        expect(result[1].secondsTilDeactivation).toBe(43200);
        expect(result[1].sceneId).toBe('1');
        expect(result[2].secondsTilActivation).toBe(43200);
        expect(result[2].secondsTilDeactivation).toBe(129600);
        expect(result[2].sceneId).toBe('7');
    });
    it('getFutureScenesFromWeek should return the future scenes from the week config for the next 86400 seconds', () => {
        const mockDate = new Date(2020, 8, 7, 12);
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            defaultSceneId: '1',
            week: [{
                    sceneId: '2',
                    startInclusive: 0,
                    endExclusive: 200
                },
                {
                    sceneId: '3',
                    startInclusive: 86400,
                    endExclusive: 86500
                },
                {
                    sceneId: '3',
                    startInclusive: 86600,
                    endExclusive: 86700
                }],
            specialDays: [],
            dayPatterns: []
        });
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.getFutureScenesFromWeek(86400);
        expect(result.length).toBe(4);
        expect(result[0].secondsTilActivation).toBe(43200);
        expect(result[0].secondsTilDeactivation).toBe(43300);
        expect(result[0].sceneId).toBe('3');
        expect(result[1].secondsTilActivation).toBe(43300);
        expect(result[1].secondsTilDeactivation).toBe(43400);
        expect(result[1].sceneId).toBe('1');
        expect(result[2].secondsTilActivation).toBe(43400);
        expect(result[2].secondsTilDeactivation).toBe(43500);
        expect(result[2].sceneId).toBe('3');
        expect(result[3].secondsTilActivation).toBe(43500);
        expect(result[3].secondsTilDeactivation).toBe(561600);
        expect(result[3].sceneId).toBe('1');
        spy.mockRestore();
    });
    it('should remove the correct items from the array', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const sceneA = { secondsTilActivation: 100, secondsTilDeactivation: 300, sceneId: 3 };
        const sceneB = { secondsTilActivation: 300, secondsTilDeactivation: 600, sceneId: 3 };
        const sceneC = { secondsTilActivation: 600, secondsTilDeactivation: 900, sceneId: 3 };
        const arrayToFilter = [sceneA, sceneB, sceneC];
        const result = timeSwitchController.removeElements(arrayToFilter, [sceneA, sceneC]);
        expect(result[0]).toBe(sceneB);
    });
    it('should return true if the current timestamp is between weekly timespan', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        Object.defineProperty(timeSwitchController, 'secondsOfWeek', {
            get: jest.fn(() => 100),
            set: jest.fn()
        });
        const timeSpan = {
            startInclusive: 0,
            endExclusive: 200,
            sceneId: '2'
        };
        expect(timeSwitchController.isWeeklyTimeSpanActive(timeSpan)).toBe(true);
    });
    it('should return true if the current timestamp is the same as the start of the weekly timespan', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        Object.defineProperty(timeSwitchController, 'secondsOfWeek', {
            get: jest.fn(() => 100),
            set: jest.fn()
        });
        const timeSpan = {
            startInclusive: 100,
            endExclusive: 200,
            sceneId: '2'
        };
        expect(timeSwitchController.isWeeklyTimeSpanActive(timeSpan)).toBe(true);
    });
    it('should return false if the current timestamp is the same as the end of the weekly timespan', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        Object.defineProperty(timeSwitchController, 'secondsOfWeek', {
            get: jest.fn(() => 200),
            set: jest.fn()
        });
        const timeSpan = {
            startInclusive: 0,
            endExclusive: 200,
            sceneId: '2'
        };
        expect(timeSwitchController.isWeeklyTimeSpanActive(timeSpan)).toBe(false);
    });
    it('should return true if the current timestamp is between specialDay timespan', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        Object.defineProperty(timeSwitchController, 'secondsOfDay', {
            get: jest.fn(() => 100),
            set: jest.fn()
        });
        const timeSpan = {
            startInclusive: 0,
            endExclusive: 200,
            sceneId: '2'
        };
        expect(timeSwitchController.isDailyTimeSpanActive(timeSpan)).toBe(true);
    });
    it('should return true if the current timestamp is the same as the start of the specialDay timespan', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        Object.defineProperty(timeSwitchController, 'secondsOfDay', {
            get: jest.fn(() => 100),
            set: jest.fn()
        });
        const timeSpan = {
            startInclusive: 100,
            endExclusive: 200,
            sceneId: '2'
        };
        expect(timeSwitchController.isDailyTimeSpanActive(timeSpan)).toBe(true);
    });
    it('should return false if the current timestamp is the same as the end of the specialDay timespan', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        Object.defineProperty(timeSwitchController, 'secondsOfDay', {
            get: jest.fn(() => 200),
            set: jest.fn()
        });
        const timeSpan = {
            startInclusive: 0,
            endExclusive: 200,
            sceneId: '2'
        };
        expect(timeSwitchController.isDailyTimeSpanActive(timeSpan)).toBe(false);
    });
    it('should return true if the current date is today', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const mockDate = new Date(2020, 8, 7, 12);
        const testDate = new Date(2020, 8, 7, 16);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.isToday(testDate);
        spy.mockRestore();
        expect(result).toBe(true);
    });
    it('should return false if the current date is not today', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController(undefined);
        const mockDate = new Date(2020, 8, 7, 12);
        const testDate = new Date(2020, 1, 7, 16);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.isToday(testDate);
        spy.mockRestore();
        expect(result).toBe(false);
    });
    it('should return the default scene when nothing else is configured', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [],
            defaultSceneId: '7',
            specialDays: [],
            dayPatterns: [],
        });
        const mockDate = new Date(2020, 8, 7, 12);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.getActiveSceneId();
        spy.mockRestore();
        expect(result).toBe('7');
    });
    it('should return the special day scene when both are configured', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [{
                    startInclusive: 0,
                    endExclusive: 604800,
                    sceneId: '7'
                }],
            defaultSceneId: '7',
            specialDays: [{
                    activationDate: new Date(2020, 8, 7),
                    dayPatternId: 1,
                    name: 'name_1'
                }],
            dayPatterns: [{
                    day: [{
                            startInclusive: 0,
                            endExclusive: 86400,
                            sceneId: '3'
                        }],
                    id: 1
                }],
        });
        const mockDate = new Date(2020, 8, 7, 12);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.getActiveSceneId();
        spy.mockRestore();
        expect(result).toBe('3');
    });
    it('should return the week scene when week is configured', () => {
        const timeSwitchController = new time_switch_controller_1.TimeSwitchController({
            week: [{
                    startInclusive: 0,
                    endExclusive: 604800,
                    sceneId: '7'
                }],
            defaultSceneId: '7',
            specialDays: [],
            dayPatterns: [],
        });
        const mockDate = new Date(2020, 8, 7, 12);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = timeSwitchController.getActiveSceneId();
        spy.mockRestore();
        expect(result).toBe('7');
    });
});
//# sourceMappingURL=time-switch.controller.test.js.map