export declare const TIME_SWITCH_OUTPUT_TOPIC = "time-switch-output-topic";
/**
 * You can define a dayPattern to represent a whole day. So you can to configure from second 0 to 86400.
 * Gaps will be filled with the defaultSceneId.
 * You can assign a dayPattern to a special day. The special day accociates the dayPattern with an date.
 * So when the configured day is active the daypattern will overwrite the normal week configuration
 */
export interface DayPattern {
    /**
     * The scenes from second 0 to 86400
     */
    day: TimeSpanWithScene[];
    /**
     * This id is required to match the pattern with a special day
     */
    id: number;
}
/**
 * A special day like `Verkaufsoffener Sonntag` or something like this.
 * You configure a day, when this special day shall be active, the id of the dayPattern which is the scene configuration and a name,
 * so you know why this special day is there
 */
export interface SpecialDay {
    /**
     * The day when the special day shall be active. Like yyyy-mm-dd
     */
    activationDate: Date;
    /**
     * The id of the dayPattern which will overwrite the week configuration
     */
    dayPatternId: number;
    /**
     * A nice name, so you know what is happening
     */
    name: string;
}
/**
 * An Object to assign a sceneId to a timeSpan
 */
export interface TimeSpanWithScene {
    /**
     * When will this scene be active?
     * This value is inclusive, so when you configure that the scene shall be active within second 100, then the scene is active after second 99
     */
    startInclusive: number;
    /**
     * When will this scene end?
     * This value is exclusive, so when you configure 100. The scene is no longer active on the 100th second
     */
    endExclusive: number;
    /**
     * the scene id which will be active in the configured time span
     */
    sceneId: string;
}
export interface TimeSwitchConfiguration {
    /**
     * The normal week configuration. You can give every second another scene if you like. You can configure timespans from 0 til 604800
     * This is the normal configuration which will be active, when no special day is active
     */
    week: TimeSpanWithScene[];
    /**
     * The default scene id. Gaps within the week and dayPattern timespans will be filled with this defaultScene index
     */
    defaultSceneId: string;
    /**
     * Accociate a daypattern with a date to overwrite the week configuration temprarily
     */
    specialDays: SpecialDay[];
    /**
     * The Daypatterns you can accociate with a special day to overwrite the week configuration temporarily
     */
    dayPatterns: DayPattern[];
}
