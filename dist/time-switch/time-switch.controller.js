"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeSwitchController = void 0;
const tslog_1 = require("tslog");
const log = new tslog_1.Logger({ name: 'TimeSwitchController' });
const ONE_WEEK_IN_SECONDS = 604800;
const ONE_DAY_IN_SECONDS = 86400;
class TimeSwitchController {
    constructor(config) {
        this.config = config;
        if (config && this.config.week !== undefined && this.config.week.length > 0) {
            this.config.week = this.fillPatternWithDefaultScene(config.week, ONE_WEEK_IN_SECONDS);
        }
    }
    setConfig(config) {
        this.config = config;
    }
    toFutureScene(timeSpan) {
        let secondsTilActivation = timeSpan.startInclusive - this.secondsOfWeek;
        if (secondsTilActivation < 0) {
            secondsTilActivation += ONE_WEEK_IN_SECONDS;
        }
        return {
            secondsTilActivation: secondsTilActivation,
            sceneId: timeSpan.sceneId,
            secondsTilDeactivation: secondsTilActivation + (timeSpan.endExclusive - timeSpan.startInclusive)
        };
    }
    specialDayToFutureScene(specialDay) {
        const secondsTilActivation = (specialDay.activationDate.getTime() - new Date().getTime()) / 1000;
        const activeDayPattern = this.config.dayPatterns.find(dayPattern => dayPattern.id === specialDay.dayPatternId);
        if (activeDayPattern) {
            activeDayPattern.day = this.fillPatternWithDefaultScene(activeDayPattern.day, ONE_DAY_IN_SECONDS);
            return activeDayPattern.day
                .map(timeSpan => {
                return {
                    secondsTilActivation: timeSpan.startInclusive + secondsTilActivation,
                    sceneId: timeSpan.sceneId,
                    secondsTilDeactivation: timeSpan.endExclusive + secondsTilActivation
                };
            })
                .sort((a, b) => a.secondsTilActivation - b.secondsTilActivation);
        }
        return [];
    }
    fillPatternWithDefaultScene(scenes, maxSecondsToFill) {
        let currentSecond = 0;
        const scenesToAdd = [];
        scenes.forEach(timeSpan => {
            if (timeSpan.startInclusive !== currentSecond) {
                scenesToAdd.push({ sceneId: this.config.defaultSceneId, startInclusive: currentSecond, endExclusive: timeSpan.startInclusive });
            }
            currentSecond = timeSpan.endExclusive;
        });
        if (scenes[scenes.length - 1].endExclusive !== maxSecondsToFill) {
            scenesToAdd.push({ sceneId: this.config.defaultSceneId, startInclusive: currentSecond, endExclusive: maxSecondsToFill });
        }
        return scenes.concat(scenesToAdd);
    }
    getFutureScenesFromSpecialDays(nextSeconds) {
        return this.config.specialDays
            .flatMap(specialDay => this.specialDayToFutureScene(specialDay))
            .sort((a, b) => a.secondsTilActivation - b.secondsTilActivation)
            .filter(a => a.secondsTilActivation < nextSeconds && a.secondsTilActivation >= 0);
    }
    getFutureScenesFromWeek(nextSeconds) {
        return this.config.week
            .map(timeSpan => this.toFutureScene(timeSpan))
            .filter(a => a.secondsTilActivation < nextSeconds)
            .sort((a, b) => a.secondsTilActivation - b.secondsTilActivation);
    }
    /**
     * Returns true if scene a is between scene b
     */
    sceneIsBetween(a, b) {
        return a.secondsTilActivation >= b.secondsTilActivation && a.secondsTilDeactivation <= b.secondsTilDeactivation;
    }
    /**
     * Returns true if scene a is starting before scene b but scene a ends within scene b
     */
    doesSceneOverlapsAtBeginning(a, b) {
        return a.secondsTilActivation < b.secondsTilActivation && a.secondsTilDeactivation <= b.secondsTilDeactivation && a.secondsTilDeactivation > b.secondsTilActivation;
    }
    /**
     * Returns true if scene a starts within scene b but runs longer then scene b
     */
    doesSceneOverlapsAtEnd(a, b) {
        return a.secondsTilActivation >= b.secondsTilActivation && a.secondsTilDeactivation > b.secondsTilDeactivation && a.secondsTilActivation < b.secondsTilDeactivation;
    }
    mergeFutureScenes(fromSpecialDays, fromWeek) {
        fromSpecialDays.forEach(specialDayScene => {
            const scenesToRemove = [];
            const scenesToAdd = [];
            fromWeek.forEach(weekScene => {
                const overlapsAtBeginning = this.doesSceneOverlapsAtBeginning(weekScene, specialDayScene);
                const overlapsAtEnd = this.doesSceneOverlapsAtEnd(weekScene, specialDayScene);
                if (this.sceneIsBetween(weekScene, specialDayScene)) {
                    scenesToRemove.push(weekScene);
                }
                else if (overlapsAtBeginning) {
                    weekScene.secondsTilDeactivation = specialDayScene.secondsTilActivation;
                }
                else if (overlapsAtEnd) {
                    weekScene.secondsTilActivation = specialDayScene.secondsTilDeactivation;
                }
                else if (weekScene.secondsTilActivation < specialDayScene.secondsTilActivation && weekScene.secondsTilDeactivation > specialDayScene.secondsTilDeactivation) {
                    scenesToRemove.push(weekScene);
                    scenesToAdd.push({ secondsTilActivation: weekScene.secondsTilActivation, secondsTilDeactivation: specialDayScene.secondsTilActivation, sceneId: weekScene.sceneId });
                    scenesToAdd.push({ secondsTilActivation: specialDayScene.secondsTilDeactivation, secondsTilDeactivation: weekScene.secondsTilDeactivation, sceneId: weekScene.sceneId });
                }
            });
            fromWeek = this.removeElements(fromWeek, scenesToRemove);
            fromWeek = fromWeek.concat(scenesToAdd);
        });
        return fromSpecialDays.concat(fromWeek);
    }
    removeElements(array, elementsToRemove) {
        return array.filter((item) => !elementsToRemove.includes(item));
    }
    getFutureScenes(nextSeconds) {
        const futureScenesFromSpecialDay = this.getFutureScenesFromSpecialDays(nextSeconds);
        const futureScenesFromWeek = this.getFutureScenesFromWeek(nextSeconds);
        if (futureScenesFromSpecialDay.length === 0)
            return futureScenesFromWeek;
        if (futureScenesFromWeek.length === 0)
            return futureScenesFromSpecialDay;
        return this.mergeFutureScenes(futureScenesFromSpecialDay, futureScenesFromWeek);
    }
    get secondsOfWeek() {
        const now = new Date();
        const dayOfWeek = (now.getDay() + 6) % 7;
        const daysInSeconds = dayOfWeek * ONE_DAY_IN_SECONDS;
        return daysInSeconds + this.secondsOfDay;
    }
    get secondsOfDay() {
        const now = new Date();
        const hoursInSeconds = now.getHours() * 60 * 60;
        const minutesInSeconds = now.getMinutes() * 60;
        return hoursInSeconds + minutesInSeconds + now.getSeconds();
    }
    isWeeklyTimeSpanActive(timeSpan) {
        return this.secondsOfWeek >= timeSpan.startInclusive && this.secondsOfWeek < timeSpan.endExclusive;
    }
    isDailyTimeSpanActive(timeSpan) {
        return this.secondsOfDay >= timeSpan.startInclusive && this.secondsOfDay < timeSpan.endExclusive;
    }
    isToday(dateToCheck) {
        const todaysDate = new Date();
        return dateToCheck.setHours(0, 0, 0, 0) == todaysDate.setHours(0, 0, 0, 0);
    }
    getActiveSceneId() {
        const specialDay = this.config.specialDays.find(specialDay => this.isToday(specialDay.activationDate));
        if (specialDay) {
            const activeDayPattern = this.config.dayPatterns.find(dayPattern => dayPattern.id === specialDay.dayPatternId);
            const activeTimeSpan = activeDayPattern.day.find(timeSpan => this.isDailyTimeSpanActive(timeSpan));
            return activeTimeSpan ? activeTimeSpan.sceneId : this.config.defaultSceneId;
        }
        const activeTimeSpan = this.config.week.find(timeSpan => this.isWeeklyTimeSpanActive(timeSpan));
        return activeTimeSpan ? activeTimeSpan.sceneId : this.config.defaultSceneId;
    }
    input(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            const activeScene = this.getActiveSceneId();
            const nextScenes = this.getFutureScenes(ONE_DAY_IN_SECONDS);
            return { activeScene: activeScene, futureScenes: nextScenes };
        });
    }
}
exports.TimeSwitchController = TimeSwitchController;
//# sourceMappingURL=time-switch.controller.js.map