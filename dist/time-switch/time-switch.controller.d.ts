import { TimeSwitchConfiguration, TimeSpanWithScene, SpecialDay } from './time-switch-config';
import { FutureScene, TimeSwitchOutput } from './time-switch.output';
export declare class TimeSwitchController {
    config: TimeSwitchConfiguration;
    constructor(config: TimeSwitchConfiguration);
    setConfig(config: TimeSwitchConfiguration): void;
    toFutureScene(timeSpan: TimeSpanWithScene): FutureScene;
    specialDayToFutureScene(specialDay: SpecialDay): FutureScene[];
    fillPatternWithDefaultScene(scenes: TimeSpanWithScene[], maxSecondsToFill: number): TimeSpanWithScene[];
    getFutureScenesFromSpecialDays(nextSeconds: number): FutureScene[];
    getFutureScenesFromWeek(nextSeconds: number): FutureScene[];
    /**
     * Returns true if scene a is between scene b
     */
    sceneIsBetween(a: FutureScene, b: FutureScene): boolean;
    /**
     * Returns true if scene a is starting before scene b but scene a ends within scene b
     */
    doesSceneOverlapsAtBeginning(a: FutureScene, b: FutureScene): boolean;
    /**
     * Returns true if scene a starts within scene b but runs longer then scene b
     */
    doesSceneOverlapsAtEnd(a: FutureScene, b: FutureScene): boolean;
    mergeFutureScenes(fromSpecialDays: FutureScene[], fromWeek: FutureScene[]): FutureScene[];
    removeElements<T>(array: T[], elementsToRemove: T[]): T[];
    getFutureScenes(nextSeconds: number): FutureScene[];
    get secondsOfWeek(): number;
    get secondsOfDay(): number;
    isWeeklyTimeSpanActive(timeSpan: TimeSpanWithScene): boolean;
    isDailyTimeSpanActive(timeSpan: TimeSpanWithScene): boolean;
    isToday(dateToCheck: Date): boolean;
    getActiveSceneId(): string;
    input(msg: any): Promise<TimeSwitchOutput>;
}
