"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (RED) {
    function MinMaxSceneMappingDashboardNode(config) {
        const node = this;
        const ui = RED.require('node-red-dashboard')(RED);
        config.tcol = ui.getTheme()['group-textColor'].value || '#1bbfff';
        // Initialize node
        RED.nodes.createNode(this, config);
        // Generate HTML/Angular code
        const html = `
            <form novalidate name="scene-mappings-form">
                <div ng-repeat="mapping in config.sceneMappings">
                    <div>Scene id: <strong>{{mapping.sceneId}}</strong></div>
                    <md-input-container>
                        <label>Min:</label>
                        <input name="min-{{mapping.scene-index}}" type="number" ng-max="mapping.maxTemp" min="0" ng-model="mapping.minTemp" ng-model-options='{ debounce: 1000, allowInvalid: true }' ng-change="mappingChanged()" required />
                        <div ng-messages="scene-mappings-form.min-{{mapping.sceneId}}.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="max">Must be smaller than {{mapping.maxTemp}}!</div>
                      </div>
                    </md-input-container>
                    <md-input-container>
                        <label>Max:</label>
                        <input name="max-{{mapping.scene-index}}" type="number" ng-min="mapping.minTemp" ng-model="mapping.maxTemp" ng-model-options='{ debounce: 1000, allowInvalid: true }' ng-change="mappingChanged()" required />
                        <div ng-messages="scene-mappings-form.max-{{mapping.sceneId}}.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="min">Must be bigger than {{mapping.minTemp}}!</div>
                        </div>
                    </md-input-container>
                    <md-input-container>
                        <div ng-click="(removeMapping(mapping))"><i class="fa fa-trash-o"></i></div>
                    </md-input-container>
                </div>
            </form>
            <form style="padding: 10px;" ng-submit="click()">
                <div>Add new Scene mapping</div>
                <md-input-container style="width: 100px">
                    <label>Scene id:</label>
                    <input name="new-scene-mapping-scene-index" ng-model="newSceneMapping.sceneId" required />
                </md-input-container>
                <md-input-container style="width: 135px">
                    <label>Min:</label>
                    <input type="number" min="0" name="new-scene-mapping-min-temp" ng-model="newSceneMapping.minTemp" required />
                </md-input-container>
                <md-input-container style="width: 135px">
                    <label>Max:</label>
                    <input type="number" min="0" name="new-scene-mapping-max-temp" ng-model="newSceneMapping.maxTemp" required />
                </md-input-container>
                <md-button type="submit" style="margin: 15px;vertical-align: top;">Add</md-button>
            </form>
        `;
        // Initialize Node-RED Dashboard widget
        // see details: https://github.com/node-red/node-red-ui-nodes/blob/master/docs/api.md
        const done = ui.addWidget({
            node: node,
            order: config.order,
            group: config.group,
            width: config.width,
            height: config.height,
            format: html,
            templateScope: 'local',
            emitOnlyNewValues: false,
            forwardInputMessages: false,
            storeFrontEndInputAsState: false,
            initController: function ($scope, _events) {
                // initialize $scope.click to send clicked widget item
                // used as ng-click="click(item, selected)"
                $scope.click = function () {
                    $scope.config.sceneMappings.push($scope.newSceneMapping);
                    $scope.newSceneMapping = undefined;
                    $scope.sendConfig();
                };
                $scope.mappingChanged = function () {
                    const inValid = $scope.config.sceneMappings.some(function (sceneMapping) {
                        return sceneMapping.minTemp > sceneMapping.maxTemp;
                    });
                    if (!inValid) {
                        $scope.sendConfig();
                    }
                };
                $scope.removeMapping = function (mapping) {
                    const index = $scope.config.sceneMappings.indexOf(mapping);
                    if (index > -1) {
                        $scope.config.sceneMappings.splice(index, 1);
                        $scope.sendConfig();
                    }
                };
                $scope.sendConfig = function () {
                    const configToSend = angular.fromJson(angular.toJson($scope.config));
                    configToSend.sceneMappings.forEach(function (sceneMapping) {
                        sceneMapping.maxTemp = sceneMapping.maxTemp * 10;
                        sceneMapping.minTemp = sceneMapping.minTemp * 10;
                    });
                    $scope.send({ payload: angular.toJson(configToSend) });
                };
                $scope.$watch('msg', function (msg) {
                    if (msg === undefined) {
                        $scope.config = {
                            sceneMappings: []
                        };
                    }
                    else {
                        $scope.config = JSON.parse(msg.items);
                        if ($scope.config.sceneMappings !== undefined) {
                            $scope.config.sceneMappings.sort(function (a, b) {
                                return a.sceneId.localeCompare(b.sceneId);
                            });
                            $scope.config.sceneMappings.forEach(function (sceneMapping) {
                                sceneMapping.maxTemp = sceneMapping.maxTemp / 10;
                                sceneMapping.minTemp = sceneMapping.minTemp / 10;
                            });
                        }
                    }
                });
            },
            convertBack: function (value) {
                return value;
            },
            beforeEmit: function (msg, value) {
                return { msg: { items: value } };
            },
            beforeSend: function (msg, orig) {
                if (orig) {
                    return orig.msg;
                }
            },
        });
        node.on('close', done);
    }
    RED.nodes.registerType('ui-min-max-scene-mapping-dashboard', MinMaxSceneMappingDashboardNode);
};
//# sourceMappingURL=min-max-scene-mapping-dashboard-node.js.map