import { TemperatureSceneMapping, TemperatureSceneMappingConfig } from './temperature-scene-mapping.config';
import { TemperatureSceneMappingOutput, FutureConfiguration } from './temperature-scene-mapping.output';
import { FutureScene, TimeSwitchOutput } from '../time-switch/time-switch.output';
export declare class TemperatureSceneMappingController {
    private config;
    readonly ONE_WEEK_IN_SECONDS = 604800;
    constructor(config: TemperatureSceneMappingConfig);
    setConfig(config: TemperatureSceneMappingConfig): void;
    getTemperatureSceneMapping(sceneId: string): TemperatureSceneMapping;
    toFutureConfiguration(futureScene: FutureScene): FutureConfiguration;
    input(msg: TimeSwitchOutput): Promise<TemperatureSceneMappingOutput>;
}
