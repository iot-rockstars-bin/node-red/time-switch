"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const temperature_scene_mapping_controller_1 = require("./temperature-scene-mapping.controller");
const time_switch_config_1 = require("../time-switch/time-switch-config");
module.exports = function (RED) {
    function TemperatureSceneMappingNode(config) {
        RED.nodes.createNode(this, config);
        config.sceneMappings = config.sceneMappings ? JSON.parse(config.sceneMappings + '') : [];
        const controller = new temperature_scene_mapping_controller_1.TemperatureSceneMappingController(config);
        this.on('input', (msg, send, done) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (msg.topic === time_switch_config_1.TIME_SWITCH_OUTPUT_TOPIC) {
                    const state = yield controller.input(msg.payload);
                    msg.payload = state;
                    send([msg, { payload: state.output, state: state }]);
                    this.context().set('state', state);
                }
                else {
                    controller.setConfig(msg.payload);
                    console.log(msg.payload);
                }
                done();
            }
            catch (err) {
                done(err);
            }
        }));
    }
    RED.nodes.registerType('temperatureSceneMapping', TemperatureSceneMappingNode);
};
//# sourceMappingURL=temperature-scene-mapping.js.map