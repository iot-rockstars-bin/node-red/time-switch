export interface TemperatureSceneMapping {
    minTemp: number;
    maxTemp: number;
    sceneId: string;
}
export interface TemperatureSceneMappingConfig {
    /**
     * The mappings between a sceneId and a min/max temp
     * For every sceneId there should be a min/max mapping!
     */
    sceneMappings: TemperatureSceneMapping[];
}
