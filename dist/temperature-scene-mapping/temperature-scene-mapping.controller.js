"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemperatureSceneMappingController = void 0;
class TemperatureSceneMappingController {
    constructor(config) {
        this.config = config;
        this.ONE_WEEK_IN_SECONDS = 604800;
    }
    setConfig(config) {
        this.config = config;
    }
    getTemperatureSceneMapping(sceneId) {
        return this.config.sceneMappings.find(sceneMapping => sceneMapping.sceneId === sceneId);
    }
    toFutureConfiguration(futureScene) {
        const sceneMapping = this.getTemperatureSceneMapping(futureScene.sceneId);
        return { max: sceneMapping.maxTemp, min: sceneMapping.minTemp, secondsTilActivation: futureScene.secondsTilActivation };
    }
    input(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            const activeSceneMapping = this.getTemperatureSceneMapping(msg.activeScene);
            const nextConfigurations = msg.futureScenes.map(futureScene => this.toFutureConfiguration(futureScene));
            return {
                min: activeSceneMapping.minTemp,
                max: activeSceneMapping.maxTemp,
                futureConfiguration: nextConfigurations
            };
        });
    }
}
exports.TemperatureSceneMappingController = TemperatureSceneMappingController;
//# sourceMappingURL=temperature-scene-mapping.controller.js.map