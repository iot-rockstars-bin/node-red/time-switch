"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const temperature_scene_mapping_controller_1 = require("./temperature-scene-mapping.controller");
describe('temperature-scene-mapping', () => {
    it('should return correct TemperatureSceneMapping', () => {
        const config = {
            sceneMappings: [
                {
                    minTemp: 120,
                    maxTemp: 200,
                    sceneId: '3'
                },
                {
                    minTemp: 130,
                    maxTemp: 160,
                    sceneId: '4'
                },
                {
                    minTemp: 140,
                    maxTemp: 150,
                    sceneId: '1'
                }
            ]
        };
        const controller = new temperature_scene_mapping_controller_1.TemperatureSceneMappingController(config);
        for (let i = 0; i < config.sceneMappings.length; i++) {
            const result = controller.getTemperatureSceneMapping(config.sceneMappings[i].sceneId);
            expect(result).toStrictEqual(config.sceneMappings[i]);
        }
    });
    it('should return the correct future configuration', () => {
        const controller = new temperature_scene_mapping_controller_1.TemperatureSceneMappingController({
            sceneMappings: [
                {
                    minTemp: 120,
                    maxTemp: 200,
                    sceneId: '3'
                }
            ]
        });
        const futureScene = controller.toFutureConfiguration({ sceneId: '3', secondsTilActivation: 123, secondsTilDeactivation: 300 });
        expect(futureScene.min).toBe(120);
        expect(futureScene.max).toBe(200);
        expect(futureScene.secondsTilActivation).toBe(123);
    });
    it('should return the correct configuration containing the configuration for the next 24 hours', () => __awaiter(void 0, void 0, void 0, function* () {
        const controller = new temperature_scene_mapping_controller_1.TemperatureSceneMappingController({
            sceneMappings: [
                {
                    minTemp: 120,
                    maxTemp: 200,
                    sceneId: '3'
                },
                {
                    minTemp: 130,
                    maxTemp: 160,
                    sceneId: '4'
                },
                {
                    minTemp: 140,
                    maxTemp: 150,
                    sceneId: '1'
                }
            ]
        });
        const configuration = yield controller.input({
            activeScene: '1',
            futureScenes: [
                {
                    secondsTilActivation: 12,
                    sceneId: '3',
                    secondsTilDeactivation: 300
                },
                {
                    secondsTilActivation: 300,
                    sceneId: '4',
                    secondsTilDeactivation: 400
                }
            ]
        });
        expect(configuration.min).toBe(140);
        expect(configuration.max).toBe(150);
        expect(configuration.futureConfiguration[0].min).toBe(120);
        expect(configuration.futureConfiguration[0].max).toBe(200);
        expect(configuration.futureConfiguration[0].secondsTilActivation).toBe(12);
        expect(configuration.futureConfiguration[1].min).toBe(130);
        expect(configuration.futureConfiguration[1].max).toBe(160);
        expect(configuration.futureConfiguration[1].secondsTilActivation).toBe(300);
    }));
});
//# sourceMappingURL=temperature-scene-mapping.controller.test.js.map