export interface TemperatureSceneMappingOutput {
    /**
     * The current min temperature
     */
    min: number;
    /**
     * The current max temperature
     */
    max: number;
    /**
     * The min/max configurations for the future
     */
    futureConfiguration: FutureConfiguration[];
}
export interface FutureConfiguration {
    /**
     * The configured min temp
     */
    min: number;
    /**
     * The configured max temp
     */
    max: number;
    /**
     * In how many seconds will these temperatures will be required?
     */
    secondsTilActivation: number;
}
